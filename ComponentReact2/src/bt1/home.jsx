import React, { Component } from "react";
import Header from "./header";
import Carousel from "./carousel";
import Product_smartphone from "./product_smartphone";
import Product_laptop from "./product_laptop";
import Promotion from "./promotion";

class Home extends Component {
  render() {
    return (
      <div className="bg-dark">
        <Header />
        <Carousel />
        <Product_smartphone />
        <Product_laptop />
        <Promotion />
      </div>
    );
  }
}

export default Home;
